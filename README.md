## Current Build Status

[![Build Status](https://travis-ci.com/KI7MT/homelab-cookbook.svg?branch=master)](https://travis-ci.com/KI7MT/homelab-cookbook)

The project is in pre-alpha stage with the majority of work focused on adding content.

## Documentation Website

The `Homelab Cookbook` is available for viewing online at [The Homelab Cookbook][] Github Site.

## What Is Homelab

One may ask, what is `Homelab`? Put simply, it is whatever you want it to be. From a single computer to
a server rack full of enterprise equipment, `Homelab` is a place to test, install, operate,
and learn all things `IT` in a safe environment where the risk to production is virtually nil.

The `Homelab Cookbook` is intended to be a guide, but is by no means an authoritative doctrine
that one must follow. However, the intention herein does aim to provide a fully functional result
if followed from start to finish.

## Intended Audience

While the subject matter used in the `Homelab Cookbook` may very from basic to advanced, the material is
suitable for anyone wishing to extend their knowledge in virtualized technologies. From beginners to seasoned
developers, there will be something for everyone.

## Content Theme

The bulk of material in the `Homelab Cookbook` is centered around Linux and it's rich ecosystem of applications
and services. While content classifiers lean more toward Developers, Software Automation, Ham Radio Enthusiasts,
and Power-Users, the overall theme is geared toward Amateur Radio as a whole and it's vast array of technical needs.
With that, most examples will attempt to provide a solution for a particular problem, extend or expand a capability,
or may introduce a new idea for others to consider.

## Work in Progress

This project is a perpetual work-in-progress as it will never be fully finished. Like any book, revision
takes the form of release editions, and so shall the `Homelab Cookbook`. As new content is added, issues
found and resolved, or better ways to accomplish a task are identified, a new edition will be tagged and
released.

## Building The Docs

To build and view the documentation locally, your system must have Python installed, preferably a later
version e.g. 3.6 or above. You will also need Git available from the command line.

```bash
# change directory to home (or wherever you want the checkout to reside)

cd ~/

# clone the repository

git clone https://github.com/KI7MT/homelab-cookbook.git

# cd into the cloned repo

cd ./homelab-cookbook

# install python dependencies

python -m pip install -r requirements.txt

# serve the documentation site

mkdocs serve

# open a browser and type the following in the address bar

http://127.0.0.1:8000

# Thats it !!

```
<div>
    <p align="center"><i>The Homelab Cookbook is <a href="https://github.com/KI7MT/homelab-cookboook/blob/master/LICENSE.md">Apache 2.0 licensed</a> code.</i></p>
</div>

[The Homelab Cookbook]: https://ki7mt.github.io/homelab-cookbook/