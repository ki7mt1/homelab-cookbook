<cite>
A hypervisor (or virtual machine monitor, VMM, virtualizer) is a kind of emulator; it is computer software, firmware or hardware that creates and runs virtual machines. A computer on which a hypervisor runs one or more virtual machines is called a host machine, and each virtual machine is called a guest machine. The hypervisor presents the guest operating systems with a virtual operating platform and manages the execution of the guest operating systems. Multiple instances of a variety of operating systems may share the virtualized hardware resources: for example, Linux, Windows, and macOS instances can all run on a single physical x86 machine. This contrasts with operating-system-level virtualization, where all instances (usually called containers) must share a single kernel, though the guest operating systems can differ in user space, such as different Linux distributions with the same kernel.

-- [Wikipedia][]

</cite> 

## Hypervisor Types

Generally speaking, there are two types of Hypervisors, Type-1 and Type-2.

Examples of Type-1 Hypervisors would include (among others):

- [Proxmox][]
- [XCP-ng][]
- [XEN Server][]
- [VMware ESXi][]
- [Microsoft Hyper-V][]

Keeping the list simple, examples of Type-2 Hypervisors would include (among others):

- [VirtualBox][]
- [VMWare Workstation / Player][]

For a full list of Type-1 and Type-2 Hypervisors, see the classification section in the [Wikipedia][] article.

## Preferred Hypervisor - Proxmox

While there is no hard-and-fast rule to use any one Hypervisor, the `Homelab Cookbook` will be using [Proxmox][]. The reasons
for this choice are:

- Installs on most all hardware commonly used in Amateur Radio (x86, arm64, armhf, etc)
- The base-OS is Debian, which has a rich support system, and is considered very stable
- Most Amateur Radio projects run on, or in some cases are based on, Debian, Ubuntu or one of it's siblings
- Ease of installation and hardware compatibility

Many of the larger, commercial based, Hypervisors require enterprise grade hardware (XEON / EPYC processors, etc). This
is particularly true for implementations like [VMware ESXI][]. While some can be made to work on consumer grade hardware, they were not designed to do so. [Proxmox][] can run on something as simple as the Raspberry PI. Again, this is not to say others cannot do the same, it's just a matter of ease of installation, usage, and hardware compatibility.

## Resources

There are many online-tutorials, and YouTube videos that detail installing [Proxmox][]. Below are some that I've found
particularly useful when first starting out with [Proxmox][].

### Learn Linux TV

`Jay LaCroix` has YouTube Channel called [LearnLinuxTV][] dedicated to all things Linux. He's also the author of a book called [Mastering Ubuntu Server][], which is a must read. I would highly recommend browsing his channel, but in particular, I found the following very helpful for [Proxmox][]:

- [Setting up your own virtualization server with Proxmox Virtual Environment](https://www.youtube.com/watch?v=MO4CaHn1EjM)
- [Building a Low Energy Virtualization Server for Your Office/Homelab with Proxmox](https://www.youtube.com/watch?v=0cN-bFZMysE)

>NOTE: Jay's video are very detailed and suitable for all levels, beginner through advanced. Also, he's in the process of creating
updated [Proxmox][] series, so be sure to subscribe to his channel for update notifications.


### Techno Tim

`Techo Tim` is another great resource on YouTube for all things virtualization. He has many videos that go over Tips & Tricks,
general installation, and advanced application installation in the [Proxmox][] world. A few I'd recommend to start off with are:

- [Proxmox VE Install and Setup Tutorial](https://www.youtube.com/watch?v=7OVaWaqO2aU&t=80s)
- [Before I do anything on Proxmox, I do this first...](https://www.youtube.com/watch?v=GoZaMgEgrHw)
- [Before you upgrade to Proxmox 7, please consider this...](https://www.youtube.com/watch?v=RCSp6gT7LWs)
- [20 Ways to Use a Virtual Machine (and other ideas for your homelab)](https://www.youtube.com/watch?v=SVQmzaSabEQ)
- [Virtualize Windows 10 with Proxmox VE](https://www.youtube.com/watch?v=6c-6xBkD2J4)

>NOTE: Using Windows10 through [Proxmox][] is very easy, though it takes a bit to get it going. Once you've loaded the
drivers, It's like running on native installation. You can also pass through things like USB sound devices, Graphics Cards,
printers and more. Once installed, use RDP to connect from any machine on your network.

### Craft Computing

`Jeff` over at [Craft Computing][] has a lot of virtualization content. From hardware reviews, builds, configuration,
server based gaming, and general discussion (including a healthy appetite for Beer !!), `Jeff` has you covered. A few notable
likes regarding [Proxmox][] are:

- [Virtualize Everything! - Proxmox Install Tutorial](https://www.youtube.com/watch?v=azORbxrItOo&list=PLGbfidALQauKd_P08DYtaGJ4qZ7l5NucR)
- [Proxmox High Availability Cluster!](https://www.youtube.com/watch?v=08b9DDJ_yf4&t=211s)
- [Proxmox vGPU Gaming Tutorial - Share Your GPU With Multiple VMs!](https://www.youtube.com/watch?v=cPrOoeMxzu0)


### Official Proxmox Sites

Documentation and video is plentiful, but here are a few I keep bookmarked:

- [Proxmox VE Official Docs](https://pve.proxmox.com/pve-docs/)
- [Proxmox VE YouTube Channel](https://www.youtube.com/user/ProxmoxVE)
- [Proxmox VE Forums](https://forum.proxmox.com/)

[Wikipedia]: https://en.wikipedia.org/wiki/Hypervisor
[Proxmox]: https://www.proxmox.com/en/
[XCP-ng]: https://xcp-ng.org/
[XEN Server]: https://xenserver.org/
[VMware ESXi]: https://www.vmware.com/products/esxi-and-esx.html
[Microsoft Hyper-V]: https://en.wikipedia.org/wiki/Hyper-V
[VirtualBox]: https://www.virtualbox.org/
[VMWare Workstation / Player]: https://www.vmware.com/products/workstation-player.html
[LearnLinuxTV]: https://www.youtube.com/c/LearnLinuxtv
[Mastering Ubuntu Server]: https://www.amazon.com/Mastering-Ubuntu-Server-configuring-troubleshooting/dp/1800564643
[Craft Computing]: https://www.youtube.com/channel/UCp3yVOm6A55nx65STpm3tXQ